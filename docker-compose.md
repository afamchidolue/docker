# Docker-compose Run multiple docker containers (mongoDB and MongoExpress)
## start mongodb and mongo-express
docker-compose -f mongo.yaml up
## access the mongo-express from browse
http://localhost:8080
 in mongo-express UI - create a new database "my-db"
 in mongo-express UI - create a new collection "users" in the database "my-db"
## start node server
cd app
npm install
node server.js
## access the nodejs application from browser
http://localhost:3000