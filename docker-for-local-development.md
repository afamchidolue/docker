# use Docker for local development
## install dependencies and start node js
cd app
npm install
node server.js
## create an image
docker pull mongo
docker pull mongo-express
## Create docker network
docker network create mongo-network 
## start mongodb
docker run -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password --name mongodb --net mongo-network mongo    
## start mongo-express
docker run -d -p 8081:8081 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=password --net mongo-network --name mongo-express -e ME_CONFIG_MONGODB_SERVER=mongodb mongo-express   
## open mongo-express from browser
http://localhost:8081
create user-account db and users collection in mongo-express
## Access you nodejs application UI from browser
http://localhost:3000
