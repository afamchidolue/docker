# Deploy Docker application on a server with Docker
## on Linux server - to add an insecure docker registry, add the file /etc/docker/daemon.json with the following content
{
  "insecure-registries" : [ "{repo-address}:{repo-port}" ]
}

# restart docker for the configuration to take affect
sudo service docker restart

# check the insecure repository was added - last section "Insecure Registries:"
docker info

# do docker login to repo
docker login {repo-address}:{repo-port}


# copy docker-compose file to remote server
scp -i ~/.ssh/id_rsa docker-compose.yaml {server-user}:{server-ip}:/home/{server-user}

# ssh into the remote server
ssh root@ip-address
# run docker compose file
docker-compose -f mongo-server.yaml up
# open port 8080 on server to access java application
