
## create a server 
ssh root@ip-address
## install docker
apt update
snap install docker
## persist the data on your local server
$ docker volume create --name nexus-data
## run the container
$ docker run -d -p 8081:8081 --name nexus -v nexus-data:/nexus-data sonatype/nexus3
## check the connectivity of the port
netstat -lpnt
docker ps
## open the created nexus
in your local machine: server-ip-adress:8081