# Dockerize Nodejs application and push to private Docker registry
## create a DOCKERFILE - refer (Dockerfile in app folder )
## build the Dockefile
docker build -t my-app:1.0 .
## Create a private register in AWS ECR
Retrieve an authentication token and authenticate your Docker client to your registry.
docker tag my-app:1.0 224099686532.dkr.ecr.us-east-1.amazonaws.com/my-app:1.0
docker push 224099686532.dkr.ecr.us-east-1.amazonaws.com/my-app:1.0